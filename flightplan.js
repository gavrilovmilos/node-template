const plan = require('flightplan')
const config = require('config').flightplan

const ENV = process.env.NODE_ENV;
const TARGET_DIRECTORY = config.target_dir;

plan.target(ENV, config.hosts);

const appName = 'RankingService';

const transferDeployData = function (local) {
  deploymentInstanceDirectory = `complan_service-${local.exec('git rev-parse HEAD').stdout.trim()}`;
  // Fetch all git tracked files
  var filesToCopy = local.exec(`git ls-files`);
  // Append local configuration file for specific environment
  filesToCopy.stdout += `\nconfig/local-${ENV}.json`;

  // Copy files to remote server.
  local.transfer(filesToCopy, `/tmp/${deploymentInstanceDirectory}`);
};

plan.local(transferDeployData);

plan.remote(function(remote) {
  remote.log('Move folder to root');
  remote.exec(`cp -fR /tmp/${deploymentInstanceDirectory} ${TARGET_DIRECTORY}`);
  remote.rm(`-rf /tmp/${deploymentInstanceDirectory}`);

  remote.exec(`ln -snf ${TARGET_DIRECTORY}/${deploymentInstanceDirectory} ${TARGET_DIRECTORY}/${appName}`);
  remote.log('Install dependencies');

  remote.exec(`npm install --prefix ${TARGET_DIRECTORY}/${appName}`, {failsafe: false});

  remote.log('Stopping aplication');
  remote.exec(`pm2 delete ${appName}`, {failsafe: true});
  remote.log('Starting aplication');
  remote.exec(`cd ${TARGET_DIRECTORY}/${appName} && export NODE_ENV=${ENV} && pm2 start index.js --name ${appName}`);
});
