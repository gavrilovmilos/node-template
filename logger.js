'use strict'

const winston = require('winston');
const expressWinston = require('express-winston');
require('winston-daily-rotate-file');
const config = require('config').get("logger");

const transports = [];

if (config.streams.console) transports.push(new (winston.transports.Console)(config.streams.console));
if (config.streams.file) transports.push(new (winston.transports.DailyRotateFile)(config.streams.file));

winston.configure({
  transports: transports,
});

const _getMessageFormat = (loggerName, message) => {
  return `${loggerName} - ${message}`;
}

module.exports = (loggerName) => {
  return {
    debug: (message) => {
      winston.log('debug', _getMessageFormat(loggerName, message));
    },

    info: (message) => {
      winston.log('info', _getMessageFormat(loggerName, message));
    },

    warn: (message) => {
      winston.log('warn', _getMessageFormat(loggerName, message));
    },

    error: (message) => {
      winston.log('error', _getMessageFormat(loggerName, message));
    },

    getExpressLogger: () => {
      if (!config.express || !config.express.streams) {
        return expressWinston.logger({transports: []});
      }

      const transports = [];
      if (config.express.streams.console) transports.push(new (winston.transports.Console)(config.express.streams.console));
      if (config.express.streams.file) transports.push(new (winston.transports.DailyRotateFile)(config.express.streams.file));

      return expressWinston.logger({transports: transports, colorize: true});
    }
  }
};
