'use strict';

const amqp = require('amqplib');

module.exports = (databaseService, rabbitConfig, loggerHandler) => {
  const logger = loggerHandler('rabbitMqService');

  const connection = amqp.connect(rabbitConfig.url);
  const rankingsQueue = rabbitConfig.queue;

  connection.then(function(conn) {
    return conn.createChannel();
  }).then(function(channel) {
    channel.assertQueue(rankingsQueue)
      .then(function (queueOk) {
        logger.info(`Connected to the RabbitMq queue[${queueOk.queue}].`);

        return channel.consume(rankingsQueue, async function (msg) {
          if (msg !== null) {
            let messageContent = msg.content.toString();
            let jsonContent = JSON.parse(messageContent);
            if (jsonContent.save) {
              logger.info(`Saving [${jsonContent.save.length}] ranks in database..`);
              await databaseService.saveMissingRankings(jsonContent.save);
              channel.ack(msg);
            } else if (jsonContent.update) {
              logger.info(`Updating rank information for user[${jsonContent.update.userId}].`);
              await databaseService.updateRank(jsonContent.update);
              channel.ack(msg);
            } else if (jsonContent.importComplete) {
              // TODO should be able to publish newly achieved ranks on rabbitMq
              logger.info(`Saving revision[${JSON.stringify(jsonContent.importComplete)}] in database...`);
              await databaseService.saveRevision(jsonContent.importComplete);
              channel.ack(msg);
            }
          }
        });
      });
  })
    .catch(console.warn);


  return {}
}