'use strict';


module.exports = (databaseService, loggerHandler) => {

  return {

    getRevision: async (revision) => {
      return databaseService.getRevision(revision);
    },

    getRankings: async () => {
      return databaseService.getRankings();
    },

    getUserRank: async (userId) => {
      return databaseService.getUserRank(userId);
    }

  }
}
