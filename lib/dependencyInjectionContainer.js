
const fnArgs = require('parse-fn-args');

module.exports = function () {
  const dependencies = {};
  const factories = {};
  const diContainer = {};

  /**
   * Register regular dependency.
   *
   * @param name {String}
   * @param dependency
   */
  diContainer.register = (name, dependency) => {
    dependencies[name] = dependency;
  }

  /**
   * Register dependency via factory function.
   * Factory function may accept parameters. Each parameter will be autowired, requirement is to have parameter names
   * that match with corresponding dependency names.
   * Autowiring will be done only once, first time when get method is called. After that instance will be saved in
   * dependencies list.
   *
   * @param name {String}
   * @param factory {function}
   */
  diContainer.factory = (name, factory) => {
    factories[name] = factory;
  }

  /**
   * Get dependency by name.
   * If dependency was injected through factory it will be created and saved in dependencies list.
   *
   * @param name
   * @returns {*}
   */
  diContainer.get = (name) => {
    if (dependencies[name]) {
      return dependencies[name];
    }

    const factory = factories[name];
    dependencies[name] = factory && _inject(factory);
    if (!dependencies[name]) {
      throw new Error(`Cannot find module with name: ${name}`);
    }
    return dependencies[name];
  }

  /**
   * Internal helper method.
   *
   * @param factory
   * @returns {*}
   * @private
   */
  const _inject = (factory) => {
    const args = fnArgs(factory)
      .map(dependency => diContainer.get(dependency));
    return factory.apply(null, args);
  }
  diContainer.dep = dependencies

  return diContainer;

}