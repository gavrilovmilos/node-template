'use strict';


const ErrorReport = function (status, code, message) {
  this.status = status;
  this.code = code;
  this.message = message;

  this.withMessage = (message) => {
    this.message = message;
    return this;
  }

  this.getStatus = function () {
    return this.status;
  }

  this.getBody = function () {
    return {
      code: code,
      message: message
    };
  }

  this.toString = function() {
    return `[ErrorReport: status[${this.status}], code[${this.code}], message[${this.message}]]`;
  }
}

const _getReport = (status, code, message) => {
  return new ErrorReport(status, code, message);
}

module.exports = () => {

  return {
    //  *********************** Util methods  ***********************
    isErrorReport: (err) => {
      if (err instanceof ErrorReport) {
        return true;
      }
      return false;
    },

    getCustomReport: (status, body) => {
      return _getReport(status, body.code ? body.code : 6000, body.message ? body.message : 'Unknown error.');
    },

    //  *********************** Report templates ***********************
    // Client errors - 400
    BAD_REQUEST: _getReport(400, 10000, 'Bad request.'),
    EMPTY_BODY: _getReport(400, 30016, 'Body is empty'),
    PERMISSION_NOT_ALLOWED: _getReport(403, 30005, 'Forbidden.'),

    // Server errors - 500
    INTERNAL_SERVER_ERROR: _getReport(500, 30502, 'Internal server error.'),
    SERVICE_UNAVAILABLE: _getReport(500, 30503, 'Service unavailable.'),

  }
}