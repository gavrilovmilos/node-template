'use strict';

const router = require('express').Router();

module.exports = (loggerHandler) => {

  router.get('/status', (req, res, next) => {
    res.status(200).send({message: 'RankingService is up and running.'});
  });

  return router
};
