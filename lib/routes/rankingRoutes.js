'use strict';

const router = require('express').Router();

module.exports = (rankingService, errorReports, loggerHandler) => {

  const logger = loggerHandler('rankingRoutes');

  router.get('/template/:route', async (req, res, next) => {

    let userId = req.params.userId;
    if (isNaN(userId)) {
      logger.error(`Could not fetch template since userId is missing`);
      return res.sendReport(errorReports.INTERNAL_SERVER_ERROR);
    }

    let data = rankingService.getRankings(userId);
    logger.info(`Template data successfully returned.`);
    res.status(200).send(data);
  });

  return router
}
