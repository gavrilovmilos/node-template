"use strict";

const mongoDb = require('mongodb');

module.exports = (dbConfig, loggerHandler) => {
  const logger = loggerHandler('databaseService');
  const mongoClient = mongoDb.MongoClient;
  const url = dbConfig.url;
  let database;
  let someCollection;
  let mongoConnected = false;

  const readyDbPromise = new Promise((resolve, reject) => {
    mongoClient.connect(url, {useNewUrlParser: true}, function (err, client) {
      if (err) {
        logger.error('Failed to connect to Mongo database due to: ' + err);
        return;
      }
      logger.info('Connected to Mongo database..');
      mongoConnected = true;
      database = client.db(dbConfig['dbName']);
      someCollection = database.collection(dbConfig.collections['collection']);
      resolve();
    });
  });

  return {

    save: async (revision) => {
      try {
        revision.createdAt = new Date();
        return await someCollection.insertOne(revision);
      } catch (err) {
        logger.error(`Could not get users from db due to error: ${err}`);
        throw new Error(err);
      }
    },

    update: async (userRank) => {
      try {
        return await someCollection.updateOne({userId: userRank.userId}, {'$set': userRank}, {upsert: true});
      } catch (err) {
        logger.error(`Could not get users from db due to error: ${err}`);
        throw new Error(err);
      }
    },

    getMany: async () => {
      try {
        return await someCollection.find({}).limit(10).toArray();
      } catch (err) {
        logger.error(`Could not get users from db due to error: ${err}`);
        throw new Error(err);
      }
    },

    getOne: async (userId) => {
      try {
        return await someCollection.findOne({userId: userId});
      } catch (err) {
        logger.error(`Could not get userRanks for user[${userId}] from db due to error: ${err}`);
        throw new Error(err);
      }
    }


  }
}