/**
 *
 * @type {*|createApplication}
 */

const express = require('express');
const diContainer = require('./dependencyInjectionContainer')();

const loggerHandler = require('../logger');
const config = require('config');
const bodyParser = require('body-parser');
const app = express();
const logger = loggerHandler('app');

app.use(logger.getExpressLogger());

diContainer.register('dbConfig', config.database);
diContainer.register('rabbitConfig', config.rabbitMq);
diContainer.register('loggerHandler', loggerHandler);
diContainer.factory('errorReports', require('./helper/errorReports'));

diContainer.factory('databaseService', require('./db/databaseService'));

diContainer.factory('rankingService', require('./service/rankingService'));
diContainer.factory('rabbitMqService', require('./service/rabbitMqService'));
diContainer.factory('rankingRoutes', require('./routes/rankingRoutes'));
diContainer.factory('adminRoutes', require('./routes/adminRoutes'));

// Express
const errorReports = diContainer.get('errorReports');
express.response.sendReport = function (errorReport) {
  if (!errorReports.isErrorReport(errorReport)) {
    errorReport = errorReports.INTERNAL_SERVER_ERROR;
  }
  return this.status(errorReport.getStatus()).send(errorReport.getBody());
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/admin', diContainer.get('adminRoutes'));
app.use('/ranking/v1', diContainer.get('rankingRoutes'));

diContainer.get('rabbitMqService')

app.listen(config.port, function () {
  logger.info(`Ranking service is listening on port ${config.port}...`);
});
